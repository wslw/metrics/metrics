#!/usr/bin/env python
import psycopg2
from jira import JIRA
from datetime import datetime, date

# Fill out URL, PROJECT, USER, PASS
# JIRA config
JIRA_URL = 'https://jira.edmi.co.uk:8443/'
JIRA_PROJECT = 'CPL'
JIRA_USER = 'santos.castane'
JIRA_PASSWORD = 'WslwSC2201'
JIRA_LABELS = []

def get_tickets(id):
    """ query data from the tickets table """
    conn = None
    found = None
    try:
        conn = psycopg2.connect(host='localhost',user='postgres',password='659459164',database='Reports')
        cur = conn.cursor()
        cur.execute("SELECT * FROM \"Tickets\" WHERE \"ID\" = "+id)
        if cur.rowcount==0:
            found = False
        else:
            found = True               
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return found


def createTask(pissue):
    sql = """INSERT INTO "Tickets" ("ID", "KEY", "Assignee", "Status", "Created_At","Updated_At")
             VALUES(%s,%s,%s,%s,%s,%s)"""
    conn = None
    assignee = None
    if pissue.fields.assignee!= None:
        assignee = pissue.fields.assignee.name
    else:
        assignee = "NoAssignee"
    try:
        conn = psycopg2.connect(host='localhost',user='postgres',password='659459164',database='Reports')
        cur = conn.cursor()
          # execute the INSERT statement
        cur.execute(sql,(pissue.id,pissue.key, assignee, pissue.fields.status.name, pissue.fields.created, pissue.fields.updated))
         # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def updateTask(pissue):
    id = pissue.id
    key = None
    assignee = None
    status = None
    updated = None
    labels = None
    sprint = None
    triagedby = None
    resolvedby = None
    resolutiondate = None
    resolution = None
    triagedfor = None
    project = None

    """ update ticket  based on the ticket id """
    sql = """ UPDATE "Tickets"
                SET "Assignee" = %s, "Status" = %s, "Updated_At" = %s, "Labels"=%s, "Sprint"=%s, "TriagedBy"=%s, "ResolvedBy"=%s, "ResolutionDate" =%s, "Resolution" = %s, "TriagedFor"=%s, "Project" = %s
                WHERE "ID" = %s"""
    conn = None
    try:
        updated= pissue.fields.updated   
        try:     
            if pissue.fields.resolution is not None:
                resolution = pissue.fields.resolution.name
                resolutiondate = pissue.fields.resolutiondate
        except:
            pass        
        
        """ try:
            for sprint in  pissue.fields.customfield_10005:
                gissue.labels = []
                if("2021_S6_C0" in sprint):
                    gissue.labels.append("2021_S6_C0")
                    if(gissue.project_id==GITLAB_PMETERS_DEV):
                        gissue.milestone_id=GITLAB_MILESTONE_DEV
                    else:
                        gissue.milestone_id=GITLAB_MILESTONE_INDEV
                # elif ("2021_S6_C0" in sprint ):


                else:
                    gissue.labels.append("No Sprint")

        except:
            gissue.labels = ["No Sprint"] """
        try:
            if pissue.fields.assignee is not None:
                assignee = pissue.fields.assignee.name
            else:
                assignee = "NoAssignee"
        except:
            assignee = "NoAssignee"

        try:
            status = pissue.fields.status.name

        except:
            status = "New"  
          
        labels = ""
        try:
            for label in  pissue.fields.labels:
                labels.append(label)  
        except:
            pass 
        for history in pissue.changelog.histories:
            for item in history.items:
                if item.field == "resolution":
                    resolvedby= history.author.name
        
        project = pissue.fields.project.key

        if pissue.fields.project.key == "CF" and pissue.fields.status.name != "NEW" and pissue.fields.status.name != "In Triage" and pissue.fields.status.name != "EXTERNALLY BLOCKED" and pissue.fields.status.name != "AWAITING FURTHER INFORMATION" and pissue.fields.status.name !="Awaiting Further Information":
            startTriage = None
            endTriage = None
            endResolved = None
            for history in pissue.changelog.histories:
                for item in history.items:
                    if item.field == "status" and item.toString == "In Triage" and item.fromString !="Triage Complete":
                        startTriage = datetime.strptime(history.created,'%Y-%m-%dT%H:%M:%S.000%z')
                    if item.field == "status" and item.toString == "Triage Complete":
                        endTriage = datetime.strptime(history.created,'%Y-%m-%dT%H:%M:%S.000%z')
                        triagedby = history.author.name
                    if item.field == "status" and (item.toString == "EXTERNAL VERIFICATION" or item.toString == "Closed"):
                        endResolved = datetime.strptime(history.created,'%Y-%m-%dT%H:%M:%S.000%z')
            if triagedby != None:
                daysTriage =endTriage-startTriage
                triagedfor=daysTriage.days
        else:
            triagedfor = None
            triagedby = None


                    

        
        conn = psycopg2.connect(host='localhost',user='postgres',password='659459164',database='Reports')
        # create a new cursor
        cur = conn.cursor()
        # execute the UPDATE  statement        
        cur.execute(sql, (assignee, status, updated, labels, sprint, triagedby, resolvedby, resolutiondate, resolution, triagedfor,project, id))        
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()





jira = JIRA({"server":'https://jira.edmi.co.uk:8443/', 'verify': False},basic_auth=(JIRA_USER, JIRA_PASSWORD))

cont = 0

while issues_chunk := jira.search_issues('order by updated DESC', startAt=cont,maxResults=200,expand="changelog"):
    for issue in issues_chunk:
        cont+=1
        pissue = issue
        try:
            found = get_tickets(issue.id)
            if found == False:
                createTask(pissue)
            else:
                updateTask(pissue)
        except:
            pass

        print("Complete: " +str(cont)+"/"+ str(issues_chunk.total))


